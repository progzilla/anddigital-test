# AND Digital Frontend Developer Coding Challenge

##Developer: 
**Cliff Pearson**
02/03/2020

##Introduction:
The purpose of this exercise is to create a HackerNews client.

##To run this code:
 - CD into the working folder
 - On first execution only, run this command: '**npm install**'
 - To run the code, run this command: '**node index**' (or '**nodemon index**', if using this process manager).
 - From the browser, enter the URL: '**localhost:3000**'

##Notes:
For the sake of brevity, I have limited this code to only treat the first 100 records returned.