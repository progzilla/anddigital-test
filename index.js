// AND Digital Frontend Developer Coding Challenge
// Cliff Pearson
// 01-03-2020

const express = require('express');
const app = express();
const Request = require('request-promise');
const moment = require('moment');

const serverPort = 8000;

const topStoriesAPIURL = 'https://hacker-news.firebaseio.com/v0/topstories.json';

//function to retrieve top stories
async function getTopStories() {

    return new Promise(function(resolve, reject) {

        console.log('Getting top stories from HackerNews');

        //get data from HackerNews API
        Request.get(topStoriesAPIURL, (error, response, body) => {
            if(error) {
                return console.log(error);
            }
            if (response.statusCode >= 200 && response.statusCode < 300) {
                resolve(JSON.parse(body));
            } else {
                reject(Error("Error!"));
            }
        });
    });
};

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

//define middleware
app.use(express.static(__dirname + '/public'));

// get an instance of the express Router
const router = express.Router();

var storyURLs = []
var stories = []

router.get('/', async function(req, res) {

    topStories = await getTopStories()
    .then(async function(topStories) {
        topStories = topStories.sort();
        topStories = topStories.reverse();
        for (let story in topStories) {
            storyAPIURL = 'https://hacker-news.firebaseio.com/v0/item/' + topStories[story] + '.json'
            storyURLs.push(storyAPIURL);
        }

        var arrayLength = storyURLs.length;
        //limit to 100 records
        if (arrayLength > 100) {
            arrayLength = 100
        };

        for (var i = 0; i < arrayLength; i++) {
            let storyURL = storyURLs[i];
            Request.get(storyURL, (error, response, body) => {
                if(error) {
                    return console.log(error);
                }
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    stories.push(JSON.parse(body));
                }
            });
        }
    })
    .then(async function() {
        res.render('stories', {
            stories: stories,
            moment: moment
        },);
    });
});

// all routes will be prefixed with /
app.use('/', router);

// start the server
app.listen(serverPort);

console.log('Server listening on port ' + serverPort);